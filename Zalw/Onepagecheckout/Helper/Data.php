<?php

namespace Zalw\Onepagecheckout\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    
    const ENABLE_ONEPAGECHECKOUT = 'zalw_onepagecheckout/general/enable_in_frontend';
    const META_TITLE = 'zalw_onepagecheckout/general/onepagecheckout_title';


    /**
     * Get enable condition
     *
     * @return boolean
     */
    public function getEnable(){
        return $this->scopeConfig->getValue(self::ENABLE_ONEPAGECHECKOUT);
    }

    /**
     * Get meta title
     *
     * @return string
     */
    public function getMetaTitle(){
        return $this->scopeConfig->getValue(self::META_TITLE);
    }

}