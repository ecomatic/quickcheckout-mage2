<?php

namespace Zalw\Onepagecheckout\Model\Payments\Paypal;

use \Magento\Paypal\Model\Config as paypalConfig;
/**
 * Paypal Configuration
 */
class Config extends paypalConfig{
	/**
	 * get Build notation code
	 * @return String 
	 */
    public function getBuildNotationCode()
    {
        return 'Zalw_SI_MagentoCE_WPS';

    }
}