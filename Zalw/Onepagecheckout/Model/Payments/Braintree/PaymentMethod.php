<?php

namespace Zalw\Onepagecheckout\Model\Payments\Braintree;

use \Magento\Braintree\Model\PaymentMethod as brainTreeConfig;

/**
 * Payment Method
 */
class PaymentMethod extends brainTreeConfig{

    /**
     * Get channel
     *
     * @return string
     */
    protected function getChannel()
    {
        return 'Magento-Zalw';
    }
}