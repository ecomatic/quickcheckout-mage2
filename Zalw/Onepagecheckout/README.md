# README #

# Magento 2.1.X Quick checkout extension #

## Description ##

Checkout has never been so fast and simplified. Quick Checkout allows you to wrap up all your checkout activities on one single page, thus reducing the time and enhancing your shopping experience.
