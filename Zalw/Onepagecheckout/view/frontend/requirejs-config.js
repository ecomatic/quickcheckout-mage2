
var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/place-order': {
                'Zalw_Onepagecheckout/js/model/place-order-with-comments-mixin': true
            }
        }
    },
    map: {
        '*': {
            'Magento_Ui/js/core/renderer/layout':'Zalw_Onepagecheckout/js/core/renderer/layout'
        }
    }
};