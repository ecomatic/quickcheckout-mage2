<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * 
 */
namespace Zalw\Onepagecheckout\Block\Account;

use Magento\Customer\Model\Url;
use Magento\Framework\View\Element\Template;

/**
 * Customer Forgotpassword
 */
class Forgotpassword extends Template
{

    /**
     * @var $_customerUrl
     */
    protected $_customerUrl;

     /**
     * @param Template\Context $context
     * @param Url $customerUrl
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Url $customerUrl,
        array $data = []
    ) {
        $this->_customerUrl = $customerUrl;
        parent::__construct($context, $data);
    }

     /**
     * Get login URL
     *
     * @return string
     */
    public function getLoginUrl()
    {
        return $this->_customerUrl->getLoginUrl();
    }
     /**
     * Get form action URL
     *
     * @return string
     */
    public function getFormActionUrl(){
        return $this->getUrl('customer/account/forgotpasswordpost', ['_secure' => true]);
    }
     /**
     * Get post URL
     *
     * @return string
     */
    public function getPostUrl(){
        return $this->getUrl('onepage/account/forgotpassword', ['_secure' => true]);
    }
}
