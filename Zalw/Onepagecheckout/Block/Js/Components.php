<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * 
 */

namespace Zalw\Onepagecheckout\Block\Js;

use Magento\Customer\Model\Url;
use Magento\Framework\View\Element\Template;

class Components extends \Magento\Framework\View\Element\Template
{
    /**
     * @var DISCOUNTS_ENABLE
     */
    const DISCOUNTS_ENABLE = 'zalw_onepagecheckout/general/onepagecheckout_discount_enable';

    /**
     * Get Discount enable
     *
     * @return integer
     */
    public function getDiscountsEnable(){
        $val = 0;
        $val = $this->_scopeConfig->getValue(self::DISCOUNTS_ENABLE);
        return ($val) ? $val : 0;
    }
}